#!/bin/bash -e
###############################################################################

hash mv
hash cmp
hash sed
hash echo
hash free
hash grep
hash lsblk
hash lscpu
hash uname
hash source
hash envsubst
hash hostname
hash systemd-detect-virt

###############################################################################

PRETTY_MOTD_TEMPLATE_BASE_DEFAULT="/usr/share/pretty-motd/templates"
PRETTY_MOTD_TEMPLATE_FILE_DEFAULT="plain.template"
PRETTY_MOTD_TEMP_FILE_DEFAULT="/tmp/pretty-motd.tmp"
PRETTY_MOTD_OUTPUT_FILE_DEFAULT="/etc/motd"
PRETTY_MOTD_OS_RELEASE_FILE_DEFAULT="/etc/os-release"
PRETTY_MOTD_STORAGE_COLUMNS_DEFAULT="NAME,HCTL,TYPE,VENDOR,MODEL,REV,TRAN,SIZE"

###############################################################################

if [ -z "${PRETTY_MOTD_TEMPLATE_BASE}"   ]; then PRETTY_MOTD_TEMPLATE_BASE="${PRETTY_MOTD_TEMPLATE_BASE_DEFAULT}"    ; fi
if [ -z "${PRETTY_MOTD_TEMPLATE_FILE}"   ]; then PRETTY_MOTD_TEMPLATE_FILE="${PRETTY_MOTD_TEMPLATE_FILE_DEFAULT}"    ; fi
if [ -z "${PRETTY_MOTD_TEMP_FILE}"       ]; then PRETTY_MOTD_TEMP_FILE="${PRETTY_MOTD_TEMP_FILE_DEFAULT}"            ; fi
if [ -z "${PRETTY_MOTD_OUTPUT_FILE}"     ]; then PRETTY_MOTD_OUTPUT_FILE="${PRETTY_MOTD_OUTPUT_FILE_DEFAULT}"        ; fi
if [ -z "${PRETTY_MOTD_OS_RELEASE_FILE}" ]; then PRETTY_MOTD_OS_RELEASE_FILE="${PRETTY_MOTD_OS_RELEASE_FILE_DEFAULT}"; fi
if [ -z "${PRETTY_MOTD_STORAGE_COLUMNS}" ]; then PRETTY_MOTD_STORAGE_COLUMNS="${PRETTY_MOTD_STORAGE_COLUMNS_DEFAULT}"; fi

###############################################################################

source "${PRETTY_MOTD_OS_RELEASE_FILE}"

###############################################################################

PRETTY_MOTD_TEMPLATE="${PRETTY_MOTD_TEMPLATE_BASE}/${PRETTY_MOTD_TEMPLATE_FILE}"

###############################################################################

export PRETTY_MOTD_SHORTNAME="$(hostname --short)"
export PRETTY_MOTD_FQDN="$(hostname --fqdn)"
export PRETTY_MOTD_PLATFORM="$(uname --kernel-name)"
export PRETTY_MOTD_DISTRIBUTION="${NAME}"
export PRETTY_MOTD_ARCHITECTURE="$(uname --machine)"
export PRETTY_MOTD_KERNEL="$(uname --kernel-release)"
export PRETTY_MOTD_HYPERVISOR="$(systemd-detect-virt || true)"
export PRETTY_MOTD_CPU="$(lscpu | sed -nr '/^Model name/ s/.*:\s*(.*)/\1/p')"
export PRETTY_MOTD_MEMORY="$(free -m  | sed -nr '/^Mem\:/ s/Mem\:\s*([0-9]+).*/\1/p')"
export PRETTY_MOTD_SWAP="$(free -m  | sed -nr '/^Swap\:/ s/Swap\:\s*([0-9]+).*/\1/p')"
export PRETTY_MOTD_STORAGE="$(lsblk --list --nodeps --noheadings --output "${PRETTY_MOTD_STORAGE_COLUMNS}")"

###############################################################################

envsubst < "${PRETTY_MOTD_TEMPLATE}" > "${PRETTY_MOTD_TEMP_FILE}"

###############################################################################

if cmp --silent "${PRETTY_MOTD_TEMP_FILE}" "${PRETTY_MOTD_OUTPUT_FILE}"; then
  rm "${PRETTY_MOTD_TEMP_FILE}"
else
  mv --force "${PRETTY_MOTD_TEMP_FILE}" "${PRETTY_MOTD_OUTPUT_FILE}"
fi

###############################################################################
